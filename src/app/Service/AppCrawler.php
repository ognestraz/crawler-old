<?php
namespace Ognestraz\Crawler\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Support\Facades\Storage;
use Ognestraz\Crawler\Extensions\Crawler;
use Ognestraz\Crawler\Models\Page;
use Ognestraz\Crawler\Models\Source;

class AppCrawler
{
    const MAX_STEP = 100;
    protected $pageList = [];
    protected $source = [];
    protected $client = null;
    
    public function __construct($id)
    {
        $this->id = $id;
        
        $this->initClient();
        $this->loadSource();
        $this->loadPageList();
    }
    
    public function initClient()
    {
        $this->client = new Client();
    }    
    
    public function loadSource()
    {
        $this->source = Source::find($this->id);
    }
    
    public function loadPageList()
    {
        $this->pageList = $this->format(Page::where('source_id', $this->id)->get());
    }
  
    protected function setSourceSettings($set)
    {
        $settings = $this->source->settings;
        $this->source->settings = array_merge($settings, $set);
        $this->source->save();
    }    
    
    public function getPageListCount()
    {
        return count($this->pageList);
    }    
    
    protected function getAuthCookie()
    {
        $response = $this->client->get('http://www.estet.ru/partner/');
        
        $crawler = new Crawler((string)$response->getBody());
        $formKey = $crawler->filter('.login-form input')->first()->attr('value');

        $jarFrom = new CookieJar();
        $jarFrom->setCookie(SetCookie::fromString($response->getHeader('set-cookie')));

        $r = $this->client->post('http://www.estet.ru/partner/customer/account/loginPost/', [
            'body' => [
                'form_key' => $formKey,
                'login[username]' => env('ESTET_LOGIN'),
                'login[password]' => env('ESTET_PASSWORD')
            ],
            'cookies' => $jarFrom
        ]);

        return $r->getHeader('set-cookie');
    }    
    
    protected function authorization($force = false)
    {
        $setCookie = $this->source->settings['cookies'];
        if ($force || !$setCookie) {
            $setCookie = $this->getAuthCookie();
            $this->setSourceSettings(['cookies' => $setCookie]);
        }
        
        $jar = new CookieJar();
        $jar->setCookie(SetCookie::fromString($setCookie));         
        return $jar;
    }    

    public function getPageList()
    {
        return $this->pageList;
    }    
    
    public function getListCatalog($status = null)
    {
        $build = Page::where('source_id', $this->id)
            ->where('catalog', true);
        
        if (null !== $status) {
            $build->where('status', $status);
        }
        
        return $build->get();
    }
    
    public function getListItem($status = null)
    {
        $build = Page::where('source_id', $this->id)
            ->where('catalog', false);
        
        if (null !== $status) {
            $build->where('status', $status);
        }
        
        return $build->get();
    }    

    
    public function format($collection)
    {
        $result = [];
        
        foreach ($collection as $item) {
            $result[$item->link] = $item;
        }
        
        return $result;
    }    

    protected function addPage($link, $data = [])
    {
        if (null === array_get($this->pageList, $link, null)) {

            $defaultData = [
                'act' => true,
                'source_id' => $this->id,
                'link' => $link,
                'status' => 'empty'
            ];
            
            $newPage = Page::create(array_merge($defaultData, $data));            
            
            if ($newPage->id) {
                $this->pageList[$newPage->link] = $newPage;
                return true;
            }
        }
        
        return false;
    }
    
    protected function checkPage($id, $filename, $status = 'success')
    {
        $page = Page::find($id);
        $page->status = $status;
        $page->filename = $filename;
        $page->save();
        
        $this->pageList[$page->link] = $page;
    }
    
    protected function parseLink($link)
    {
        $temp = explode('?', $link);
        return $temp[0];        
    }    
    
    protected function getEmptyPage()
    {
        return array_filter($this->pageList, function ($var) {
            return 'empty' === $var->status;
        });
    }
    
    protected function getPaginationPages($content)
    {
        $pagination = $this->source->settings['pagination'];
        $startPage = $this->parseLink($this->source->settings['start']);

        $crawler = new Crawler($content);
        $parseSource = array_unique($crawler->filter($pagination)->extract(['href']));

        return array_map(function ($a) use ($startPage) {
            
            if (0 === strpos($a, 'http://')) {
                return $a;
            } elseif (0 === strpos($a, '/')) {
                return $a;
            }
            
            return $startPage . $a;
        }, $parseSource);
    }    
    
    protected function getFullLink($link)
    {
        $domain = $this->source->domain;
        if (0 === strpos($link, 'http://')) {
            return $link;
        } elseif (0 === strpos($link, '/')) {
            return 'http://' . $domain . $link;
        } else {
            return 'http://' . $domain . '/' . $link;
        }            
    }
    
    protected function getResponse($link, $ajax = false)
    {
        $options = [];
        if (isset($this->source->settings['auth']) && $this->source->settings['auth']) {
            $options['cookies'] = $this->authorization();
        }
        
        if ($ajax) {
            $options['headers']['X-Requested-With'] = 'XMLHttpRequest';
        }
        
        try {
            $res = $this->client->get($link, $options);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $options['cookies'] = $this->authorization(true);
            $res = $this->client->get($link, $options);           
        }
        
        return $res;        
    }
    
    protected function getContent($link, $ajax = false)
    {
        $response = $this->getResponse($this->getFullLink($link), $ajax);
        return (string)$response->getBody();        
    }    
    
    protected function saveContent($link, $content)
    {
        $filename = substr(md5($link), 20);
        Storage::disk('local')->put($filename, $content);
        return $filename;
    }
    
    protected function saveResult($result)
    {
        $content = json_encode($result);
        Storage::disk('local')->put('source_' . $this->id, $content);
    }
    
    protected function getResult()
    {
        return Storage::disk('local')->get('source_' . $this->id);
    }    
    
    public function parsingPages()
    {
        $i = 0;
        while ($emptyPages = $this->getEmptyPage()) {
            
            if ($i++ > self::MAX_STEP) {
                break;
            }
 
            $nowLink = current($emptyPages);
            $content = $this->getContent($nowLink->link);
            
            $filename = $this->saveContent($nowLink->link, $content);
            $this->checkPage($nowLink->id, $filename);

            $parseSource = $this->getPaginationPages($content);
            
            foreach ($parseSource as $link) {
                $this->addPage($link, ['catalog' => true]);
            }
        }
    }
    
    public function getOnePage($link, $settings = [])
    {
        $content = $this->getContent($link, true);
        
        $block = $this->source->settings['block'];
        $parse = json_decode($this->source->settings['parse'], true);
        
        $crawler = new Crawler($content);
        $parseData = $crawler->filter($block)->parseData($parse);
        
        return $this->applyFilter($parseData);
    }
    
    public function reset() 
    {
        return Page::where('source_id', $this->id)->delete();
    }    
    
    public function run($source = '') 
    {
        $startPage = $source ? $source : $this->source->settings['start'];
        $this->addPage($startPage, ['catalog' => true]);
        
        $this->parsingPages();
        
        return $this->source;
    }
    
    public function stop() 
    {
        return true;
    }
    
    protected function applyFormat($value, $format) 
    {
        $resultValue = $value;
        
        foreach (str_split($format) as $f) {
            switch ($f) {
                case 'i': $resultValue = preg_replace('/\D/', '', $resultValue); break;
                case 'l': $resultValue = preg_replace('/[\s]{2,}/', ' ', str_replace("\n", ' ', $resultValue)); break;
                default: break;
            }
        }

        return $resultValue;
    }    
    
    protected function getFiltered($filter, $item)
    {
        $key = key($filter);
        
        if (true === array_key_exists($key, $item)) {
            $pattern = current($filter);
            if ($pattern) {
                preg_match_all($pattern, $item[$key], $matches);
                if (isset($matches[0]) && isset($matches[0][0])) {
                    return $matches[0][0];
                }
            } else {
                return $item[$key];
            }
            return '';
        }
        
        return '';
    }
    
    protected function getTags($value, $item)
    {
        $tags = [];    
        foreach ($item as $key => $v) {
            $tags['{' . $key . '}'] = $v;
        }
        return strtr($value, $tags);
    }
    
    protected function applyFilter($data, $catalog = false)
    {
        $result = [];
        $filter = json_decode($this->source->settings[$catalog ? 'filter_cat' : 'filter'], true);

        foreach ($data as $item) {
            $temp = [];
            foreach ($filter as $key => $f) {
                $t = explode(':', $key);
                $k = $t[0];
                $itemTemp = array_merge($item, $temp);

                $temp[$k] = is_string($f) ? $this->getTags($f, $itemTemp) : $this->getFiltered($f, $itemTemp);
                $temp[$k] = trim($temp[$k]);
                if (isset($t[1])) {
                    $temp[$k] = $this->applyFormat($temp[$k], $t[1]);
                }
            }

            $result[] = $temp;
        }
        return $result;
    }
    
    public function parseItem() 
    {
        $data = [];
        
        $domain = $this->source->domain;
        $block = $this->source->settings['block'];
        $parse = json_decode($this->source->settings['parse'], true);
        
        $list = $this->getListItem('success');
        
        foreach ($list as $page) {
            $file = Storage::disk('local')->get($page->filename);

            $crawler = new Crawler($file);
            $parseData = $crawler->filter($block)->parseData($parse);
            if (isset($parseData[0])) {
                $parseData[0][':source'] = $page->link;
                $parseData[0][':domain'] = $domain;
            }
            
            $data = array_merge($data, $parseData);
        }
        
        return $this->applyFilter($data);
    }
    
    public function parse($start = null, $end = null)
    {
        $data = [];
        
        $block = $this->source->settings['block_cat'];
        $parse = json_decode($this->source->settings['parse_cat'], true);
        
        $list = [];
        if (null !== $start) {
            $list = $end ? array_slice($this->pageList, $start, $end) : array_slice($this->pageList, $start);
        } else {
            $list = $this->pageList;
        }
        
        foreach ($list as $page) {
            $file = Storage::disk('local')->get($page->filename);

            $crawler = new Crawler($file);
            $parseData = $crawler->filter($block)->parseData($parse);
            
            foreach ($parseData as $item) {
                if (isset($item['item'])) {
                    $link = $this->parseLink($page->link) . $item['item'];
                    $this->addCatalogPages($link);
                }
            }

            $data = array_merge($data, $parseData);
        }
        
        $result = $this->applyFilter($data, true);
        $this->saveResult($result);
        
        return $result;
    }    
    
    public function addCatalogPages($link, $data = []) 
    {
        $defaultData = [
            'act' => true,
            'catalog' => false,
            'source_id' => $this->id,
            'link' => $link,
            'status' => 'empty'
        ];        

        $resultData = array_merge($defaultData, $data);
        
        $pageCount = Page::where('link', $resultData['link'])->count();
        if (!$pageCount) {
            return Page::Create($resultData);
        }
        
        return false;
    }
    
    public function state($catalog = false)
    {
        $catalogCount = Page::where('source_id', $this->id)
                ->where('status', 'empty')
                ->where('catalog', true)
                ->count();

        if ($catalogCount) {
            $catalog = true;
        }

        $data = [
            'page' => [
                'all' => Page::where('source_id', $this->id)
                    ->where('catalog', $catalog)
                    ->count(),
                'empty' => Page::where('source_id', $this->id)
                    ->where('catalog', $catalog)
                    ->where('status', 'empty')
                    ->count(),
                'success' => Page::where('source_id', $this->id)
                    ->where('catalog', $catalog)
                    ->where('status', 'success')
                    ->count()
            ]
        ];
        
        return $data;
    }
    
    public function result()
    {
        return json_decode($this->getResult(), true);
    }
}
