<?php
namespace Ognestraz\Crawler\Extensions;

class Crawler extends \Symfony\Component\DomCrawler\Crawler
{
    /**
     * 
     * @param array|string $attributes
     * @param bool $key
     * @return array
     */
    public function attrs($attributes, $key = true)
    {
        $list = [];
        $one = false;
        if (true === is_string($attributes)) {
            $list[] = $attributes;
            $one = true;
        } else {
            $list = $attributes;
        }
        
        $data = [];
        foreach ($list as $attribute) {
            if ('_text' === $attribute) {
                $data[$attribute] = parent::text();
            } else if ('_html' === $attribute) {
                $data[$attribute] = parent::html();
            } else {
                $data[$attribute] = parent::attr($attribute);
            }
        }
        
        return true === $one ? current($data) : $data;
    }
    
    /**
     * 
     * @param array $attributes
     * @param bool $key
     * @return array
     */
    public function parseData(array $pattern)
    {
        return $this->each(function (Crawler $node) use ($pattern) {
            $result = [];
            foreach ($pattern as $key => $subPattern) {
                $subKey = key($subPattern);
                $attrs = $subPattern[$subKey];
                
                $data = $node->filter($subKey)->each(function (Crawler $node) use ($attrs) {
                    return $node->attrs($attrs);
                });
                
                $result[$key] = implode("\n", $data);
            }
            
            return $result;
        });        
    }    
}

