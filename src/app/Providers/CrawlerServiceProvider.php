<?php
namespace Ognestraz\Crawler\Providers;

use Illuminate\Support\ServiceProvider;

class CrawlerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__.'/../Http/routes.php';
        }
        
        $pathViews = __DIR__.'/../../resources/views';
        $packageName = 'crawler';
        $this->loadViewsFrom($pathViews, $packageName);

        $pathDatabase = __DIR__.'/../../database/';
        $pathMigrations = $pathDatabase . 'migrations';
        $pathSeeds = $pathDatabase . 'seeds';

        $this->publishes([
            $pathViews => base_path('resources/views/vendor/' . $packageName),
            $pathMigrations => base_path('database/migrations'),
            $pathSeeds => base_path('database/seeds')            
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
