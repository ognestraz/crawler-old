<?php namespace Ognestraz\Crawler\Http\Controllers;

use Illuminate\Http\Request;
use Ognestraz\Crawler\Models\Project;
use Ognestraz\Crawler\Models\Source;

class ProjectController extends Controller
{
    protected $modelName = 'project';

    public function index()
    {
        $list = Project::orderBy('sort', 'asc')->get();

        return view('crawler::project', array('list' => $list));
    }

    public function show($id)
    {
        $model = $this->model($id);
        $sources = Source::where('project_id', $id)->get();
        
        return view('crawler::project-source', [
            $this->modelName => $model,
            'sources' => $sources
        ]);
    }     
    
}
