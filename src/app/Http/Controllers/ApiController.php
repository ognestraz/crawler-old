<?php namespace Ognestraz\Crawler\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ognestraz\Crawler\Service\AppCrawler;

class ApiController extends Controller
{
    public function projectAction(Request $request, $id, $make)
    {
    }
    
    public function sourceAction(Request $request, $id, $make)
    {
        $export = $request->get('export', false);
        
        $crawler = new AppCrawler($id);
        $res = $crawler->$make();
        
        if ($export) {
            return new JsonResponse($res, JsonResponse::HTTP_OK, [
                'Content-Type' => 'application/json',
                'Content-Disposition' => 'attachment; filename="export.json"'
            ]);
        }

        return new JsonResponse($res);
    }    
}
