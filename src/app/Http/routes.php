<?php

Route::group([
    'namespace' => 'Ognestraz\Crawler\Http\Controllers'
    ], function () {
        Route::resource('project', 'ProjectController');        
        Route::resource('source', 'SourceController');        
});


Route::group([
    'namespace' => 'Ognestraz\Crawler\Http\Controllers',
    'prefix' => 'api/v1/'
    ], function () {
        Route::get('project/{id}/{action}', array('uses' => 'ApiController@projectAction'));
        Route::get('source/{id}/{action}', array('uses' => 'ApiController@sourceAction'));
});