<?php namespace Ognestraz\Crawler\Models;

class Page extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'page';
    
    protected $fillable = ['link', 'act', 'source_id', 'filename', 'catalog', 'status'];

}
