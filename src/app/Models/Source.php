<?php namespace Ognestraz\Crawler\Models;

class Source extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'source';

    public function setSettingsAttribute($settings)
    {
        $this->attributes['settings'] = serialize($settings);
    }  

    public function getSettingsAttribute($value)
    {
        return is_string($value) ? unserialize($value) : [];
    }      
}
