<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Ognestraz\Crawler\Models\Project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseModel::unguard();

        $settings = serialize([]);
        
        $listProject = [
            ['id' => 1, 'name' => 'PayCosplay.ru', 'domain' => 'paycosplay.ru', 'settings' => $settings]
        ];
        
        $dataBase = [
            'act' => true,
            'status' => 'empty',
            'user_id' => 1
        ];

        DB::table((new Project())->getTable())->truncate();

        $i = 0;
        foreach ($listProject as $row) {
            $row['sort'] = $i++;
            Project::create(array_merge($dataBase, $row));
        }
    }
}
