<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Ognestraz\Crawler\Models\Source;

class SourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseModel::unguard();

        $settings = serialize([]);
        
        $listSource = [
            ['id' => 1, 'project_id' => 1, 'name' => 'Karnaval-Prokat', 'domain' => 'karnaval-prokat.ru', 'settings' => $settings]
        ];
        
        $dataBase = [
            'act' => true,
            'status' => 'empty'
        ];

        DB::table((new Source())->getTable())->truncate();

        $i = 0;
        foreach ($listSource as $row) {
            $row['sort'] = $i++;
            Source::create(array_merge($dataBase, $row));
        }
    }
}
